1. VirtualBox-6.1-0:6.1.36_152435_fedora36-1.x86_64

2. aajohan-comfortaa-fonts-0:3.101-4.fc36.noarch

3. alacritty-0:0.10.1-1.fc36.x86_64

4. bat-0:0.18.3-3.fc36.x86_64

5. bison-0:3.8.2-2.fc36.x86_64

6. brave-browser-0:1.41.99-1.x86_64

7. cargo-0:1.62.0-1.fc36.x86_64

8. chkconfig-0:1.19-2.fc36.x86_64

8. clang-0:14.0.0-1.fc36.x86_64

9. cmake-0:3.22.2-1.fc36.x86_64

10. code-0:1.69.2-1658162074.el7.x86_64

11. codium-0:1.69.2-1658192401.el7.x86_64

12. containerd.io-0:1.6.6-3.1.fc36.x86_64

13. dkms-0:3.0.5-1.fc36.noarch

14. docker-ce-3:20.10.17-3.fc36.x86_64

15. docker-ce-cli-1:20.10.17-3.fc36.x86_64

16. docker-compose-plugin-0:2.6.0-3.fc36.x86_64

17. dracut-live-0:056-1.fc36.x86_64

18. elfutils-libelf-devel-0:0.187-4.fc36.x86_64

19. exa-0:0.10.1-4.fc36.x86_64

20. extra-cmake-modules-0:5.96.0-1.fc36.noarch

21. fd-find-0:8.2.1-5.fc36.x86_64

22. fedora-release-kde-0:36-18.noarch

23. figlet-0:2.2.5-23.20151018gita565ae1.fc36.x86_64

24. fish-0:3.5.0-1.fc36.x86_64

25. flex-0:2.6.4-10.fc36.x86_64

26. fuse-0:2.9.9-14.fc36.x86_64

27. git-0:2.37.1-1.fc36.x86_64

28. gitui-0:0.20.1-1.fc36.x86_64

29. google-noto-emoji-fonts-0:20211102-1.fc36.noarch

30. kde-l10n-0:17.08.3-12.fc36.noarch

31. kdecoration-devel-0:5.25.3-1.fc36.x86_64

32. kf5-kconfigwidgets-devel-0:5.96.0-1.fc36.x86_64

33. kf5-kdeclarative-devel-0:5.96.0-1.fc36.x86_64

34. kf5-ki18n-devel-0:5.96.0-1.fc36.x86_64

35. kf5-kitemmodels-devel-0:5.96.0-1.fc36.x86_64

36. kf5-kwayland-devel-0:5.96.0-1.fc36.x86_64

37. kf5-plasma-devel-0:5.96.0-1.fc36.x86_64

38. langpacks-en-0:3.0-23.fc36.noarch

39. libSM-devel-0:1.2.3-10.fc36.x86_64

40. libreoffice-draw-1:7.3.4.2-4.fc36.x86_64

41. libreoffice-math-1:7.3.4.2-4.fc36.x86_64

42. lightly-0:0.4.1-3.fc36.x86_64

43. lolcat-0:1.2-4.fc36.x86_64

44. mediawriter-0:5.0.2-1.fc36.x86_64

45. neovim-0:0.7.2-1.fc36.x86_64

46. nodejs-1:16.14.0-2.fc36.x86_64

47. openssl-devel-1:3.0.5-1.fc36.x86_64

48. p7zip-0:16.02-22.fc36.x86_64

49. plasma-workspace-devel-0:5.25.3.1-1.fc36.x86_64

50. procs-0:0.10.10-4.fc36.x86_64

51. qt5-qtdeclarative-devel-0:5.15.3-2.fc36.x86_64

52. qt5-qtx11extras-devel-0:5.15.3-1.fc36.x86_64

53. ripgrep-0:13.0.0-4.fc36.x86_64

54. rpmfusion-free-release-0:36-1.noarch

55. rpmfusion-nonfree-release-0:36-1.noarch

56. starship-0:1.2.1-2.fc36.x86_64

57. tealdeer-0:1.4.1-9.fc36.x86_64

58. tlp-0:1.5.0-3.fc36.noarch

59. tlp-rdw-0:1.5.0-3.fc36.noarch

60. tokei-0:12.0.4-11.fc36.x86_64

61. unrar-0:6.1.7-1.fc36.x86_64

62. vagrant-0:2.2.19-4.fc36.noarch

63. vim-enhanced-2:9.0.049-1.fc36.x86_64

64. zlib-devel-0:1.2.11-31.fc36.x86_64

and Some packages installed using Cargo (rust package managaer)
65. du-dust v0.8.1:
    dust
66. zellij v0.30.0:
    zellij
